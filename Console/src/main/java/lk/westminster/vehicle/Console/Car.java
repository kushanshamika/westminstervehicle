package lk.westminster.vehicle.Console;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collation = "vehicle")
public class Car extends Vehicle {

    private String vehiclePaint;
    private String noOfSeats;
    private String vehicleMake;

    public Car(String vehicleType, String plateNumber, String vehicleModel, String engineCapacity, boolean availability, String fuelType, double pricePerKM, String vehiclePaint, String noOfSeats, String vehicleMake) {
        super(vehicleType, plateNumber, vehicleModel, engineCapacity, availability, fuelType, pricePerKM);
        this.vehiclePaint = vehiclePaint;
        this.noOfSeats = noOfSeats;
        this.vehicleMake = vehicleMake;
    }

    public String getVehiclePaint() {
        return vehiclePaint;
    }

    public void setVehiclePaint(String vehiclePaint) {
        this.vehiclePaint = vehiclePaint;
    }

    public String getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(String noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    @Override
    public String toString() {
        return "Car{" +super.toString()+
                "vehiclePaint='" + vehiclePaint + '\'' +
                ", noOfSeats='" + noOfSeats + '\'' +
                ", vehicleMake='" + vehicleMake + '\'' +
                '}';
    }
}

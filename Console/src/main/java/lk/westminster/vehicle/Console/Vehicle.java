package lk.westminster.vehicle.Console;


import org.springframework.data.annotation.Id;

public abstract class Vehicle {


    private String vehicleType;
    private String plateNumber;
    private String vehicleModel;
    private String engineCapacity;
    private boolean availability;
    private String fuelType;
    private double pricePerKM;

    public Vehicle(String vehicleType, String plateNumber, String vehicleModel, String engineCapacity, boolean availability, String fuelType, double pricePerKM) {
        this.vehicleType = vehicleType;
        this.plateNumber = plateNumber;
        this.vehicleModel = vehicleModel;
        this.engineCapacity = engineCapacity;
        this.availability = availability;
        this.fuelType = fuelType;
        this.pricePerKM = pricePerKM;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public double getPricePerKM() {
        return pricePerKM;
    }

    public void setPricePerKM(double pricePerKM) {
        this.pricePerKM = pricePerKM;
    }

    @Override
    public String toString() {
        return
                "vehicleType='" + vehicleType + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                ", vehicleModel='" + vehicleModel + '\'' +
                ", engineCapacity='" + engineCapacity + '\'' +
                ", availability=" + availability +
                ", fuelType='" + fuelType + '\'' +
                ", pricePerKM=" + pricePerKM;
    }
}

package lk.westminster.vehicle.Console;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.ArrayList;


public interface VehicleRepository extends MongoRepository<Vehicle,String> {

    @Query("{vehicleType:'?0'}")
    ArrayList<Vehicle> findVehicle(String vehicleType);

    @Query("{plateNumber:'?0'}")
    ArrayList<Book> Vehicle(String plateNumber);

}

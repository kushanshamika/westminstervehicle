package lk.westminster.vehicle.Console;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/api")
public class API {

    @Autowired
    private VehicleRepository repository;

    @Autowired
    private  BookRepository bookRepository;


    @GetMapping(value = "/")
    public List<Vehicle> getAll(){
        return repository.findAll();
    }

    @GetMapping(value = "/vehicle")
    @ResponseBody
    public ArrayList<Vehicle> vehicle(@RequestParam String plateNumber){

        ArrayList<Vehicle> vehicle = vehicle(plateNumber);
        return vehicle;
    }

    @GetMapping(value = "/book")
    @ResponseBody
    public boolean book(@RequestParam String plateNumber, @RequestParam String startDate, @RequestParam String endDate){

        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        while (!start.isAfter(end)) {

            bookRepository.save(new Book(plateNumber,start));
            start = start.plusDays(1);
        }



        return true;
    }

    @GetMapping(value = "/search")
    @ResponseBody
    public ArrayList<Vehicle> search(@RequestParam String vehicleType,@RequestParam String startDate,@RequestParam String endDate ){

        ArrayList<Vehicle> vehicles = repository.findVehicle(vehicleType);
        ArrayList<Vehicle> availableVeh= new ArrayList<>();
        ArrayList<Book> books;
ArrayList <LocalDate> bookReqDates =new ArrayList<>();
int status=1;

        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        while (!start.isAfter(end)) {
            bookReqDates.add(start);
            start = start.plusDays(1);
        }


        for (Vehicle veh:vehicles) {
            books = bookRepository.findVehicle(veh.getPlateNumber());
            for (Book st:books
                 ) {
                //String plateBack=st.getPlateNumber();
                LocalDate booked=st.getDate();
                if(bookReqDates.contains(booked)){
                    status=0;
                }

            }
            if(status==1){
                availableVeh.add(veh);
            }
            status=1;

        }



        return availableVeh;
    }
}

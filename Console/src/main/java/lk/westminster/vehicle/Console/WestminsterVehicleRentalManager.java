package lk.westminster.vehicle.Console;

import java.util.ArrayList;
import java.util.List;


public class WestminsterVehicleRentalManager {


    public ArrayList<Vehicle> vehicles = new ArrayList<>();


    public void addVehicle(Vehicle vehicle){
        vehicles.add(vehicle);
    }

    public int listSize(){
        return vehicles.size();
    }

    public void deleteVehicle(int index){

        vehicles.remove(index);

    }

    public void vehicleList(){

        int index = 0;
        for (int i =0;i<vehicles.size();i++){
            System.out.println(i+" "+vehicles.get(i)+"\n");
        }

    }

    public Vehicle getObject(int index){

        return vehicles.get(index);
    }
}

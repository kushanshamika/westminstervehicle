package lk.westminster.vehicle.Console;

import java.time.LocalDate;

public class Book {

    private String plateNumber;
    private LocalDate date;

    public Book(String plateNumber, LocalDate date) {
        this.plateNumber = plateNumber;
        this.date = date;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Book{" +
                "plateNumber='" + plateNumber + '\'' +
                ", date=" + date +
                '}';
    }
}

package lk.westminster.vehicle.Console;

import com.mongodb.DBCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.FileWriter;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

@SpringBootApplication
public class ConsoleApplication implements CommandLineRunner {

	@Autowired
	VehicleRepository repository;


	private int option;
	private String plateNumber;
	private String vehicleModel;
	private String engineCapacity;
	private String fuelType;
	private double pricePerKM;
	private String vehicleMake;
	private String vehiclePaint;
	private String noOfSeats;
	private String bikeType;
	private String startType;

	public static void main(String[] args) {
		SpringApplication.run(ConsoleApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		WestminsterVehicleRentalManager vehicleRentalManager = new WestminsterVehicleRentalManager();



		repository.findAll().forEach(u -> vehicleRentalManager.addVehicle(u));

		Scanner input = new Scanner(System.in);


		while (true){

			System.out.println("Choose Option to Proceed\n\t1.\tLaunch GUI\n\t2.\tAdd Vehicle\n\t3.\tPrint Vehicle List\n\t4.\tDelete Vehicle\n\t5.\tSave\n\t6.\tQuit");

			option = input.nextInt();

			if (option == 1){

				Runtime rt = Runtime.getRuntime();
				String url = "http://localhost:4200";
				rt.exec("open " + url);

			}else if (option == 2){

				if (vehicleRentalManager.listSize() > 50){
					System.out.println("Reach the maximum vehicle number");
					continue;
				}else {

				}

				System.out.println("Choose Vehicle Category\n\t1.\tCar\n\t2.\tMotor Bike");
				option = input.nextInt();

				if (option == 1 || option == 2){

					System.out.println("Enter Plate Number");
					input.nextLine();
					plateNumber = input.nextLine();

					System.out.println("Enter Vehicle Model");
					vehicleModel = input.nextLine();

					System.out.println("Enter Engine Capacity");
					engineCapacity = input.nextLine();

					System.out.println("Enter Price per KM");
					pricePerKM = input.nextDouble();

					System.out.println("Enter Fuel Type");
					input.nextLine();
					fuelType = input.nextLine();

					if (option == 1){

						System.out.println("Enter Seat Capacity");
						noOfSeats = input.nextLine();

						System.out.println("Enter Vehicle Make");
						vehicleMake = input.nextLine();

						System.out.println("Enter Vehicle Paint");
						vehiclePaint = input.nextLine();

						Vehicle car = new Car("1",plateNumber,vehicleModel,engineCapacity,true,fuelType,pricePerKM,vehiclePaint,noOfSeats,vehicleMake);
						vehicleRentalManager.addVehicle(car);
						repository.save(car);
						System.out.println("You have successfully inserted");


					}else {

						System.out.println("Enter Bike Type");
						bikeType = input.nextLine();

						System.out.println("Enter Start Type");
						startType = input.nextLine();

						Vehicle bike = new MotorBike("0",plateNumber,vehicleModel,engineCapacity,true,fuelType,pricePerKM,bikeType,startType);
						vehicleRentalManager.addVehicle(bike);
						repository.save(bike);
					}

				}else {
					System.out.println("Invalid Code");
				}
			}else if (option == 3){

				vehicleRentalManager.vehicleList();


			}else if (option == 4){

				System.out.println("Enter Vehicle Index");
				int index = input.nextInt();
				vehicleRentalManager.deleteVehicle(index);
				repository.deleteAll();
				for (int i=0;i<vehicleRentalManager.listSize();i++){
					repository.save(vehicleRentalManager.getObject(i));
				}


			}else if (option == 5){

				try{
					FileWriter fw=new FileWriter("fileWrite.txt");
					Iterator iterator = vehicleRentalManager.vehicles.iterator();
					while (iterator.hasNext()){
						fw.write(iterator.next().toString()+"\n");
					}
					fw.close();
				}catch(Exception e){System.out.println(e);}

			}else if (option == 6){

				System.exit(1);
			}



		}
	}
}

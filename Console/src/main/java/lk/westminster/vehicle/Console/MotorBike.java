package lk.westminster.vehicle.Console;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collation = "vehicle")
public class MotorBike extends Vehicle {

    private String bikeType;
    private String startType;


    public MotorBike(String vehicleType, String plateNumber, String vehicleModel, String engineCapacity, boolean availability, String fuelType, double pricePerKM, String bikeType, String startType) {
        super(vehicleType, plateNumber, vehicleModel, engineCapacity, availability, fuelType, pricePerKM);
        this.bikeType = bikeType;
        this.startType = startType;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public String getStartType() {
        return startType;
    }

    public void setStartType(String startType) {
        this.startType = startType;
    }

    @Override
    public String toString() {
        return "MotorBike{" + super.toString()+
                "bikeType='" + bikeType + '\'' +
                ", startType='" + startType + '\'' +
                '}';
    }
}

package lk.westminster.vehicle.Console;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.ArrayList;

public interface BookRepository extends MongoRepository<Book,String> {

    @Query("{plateNumber:'?0'}")
    ArrayList<Book> findVehicle(String plateNumber);
}

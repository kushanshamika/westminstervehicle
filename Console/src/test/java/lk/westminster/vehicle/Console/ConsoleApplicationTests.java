package lk.westminster.vehicle.Console;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ConsoleApplicationTests {

}

class unitTest{

	private WestminsterVehicleRentalManager rentalManager;

	@BeforeEach
	void initEach() {rentalManager = new WestminsterVehicleRentalManager();}

	@Test
	void testArraySize() {
		assertEquals(0, rentalManager.listSize(),
				"Testing Vehicle Array Size");
	}

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {HttpClient} from '@angular/common/http';


interface Vehicle {
  vehicleType:String;
  plateNumber:String;
  vehicleModel: String;
  engineCapacity:String;
  availability:string;
  fuelType:String;
  pricePerKM: string;
  vehiclePaint:String;
  noOfSeats: string;
  vehicleMake: string;
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  vehicles:Vehicle[]=[]
  

  constructor(public route: ActivatedRoute,private http:HttpClient,public router:Router) { }
  plateNumber:String;
  startDate:String;
  endDate:String;
  status:boolean;

  ngOnInit() {

    this.route.queryParams.subscribe(params =>{

      this.plateNumber = params["plateNumber"];
      this.startDate = params["startDate"];
      this.endDate = params["endDate"]
    });

    var url = "http://localhost:8080/api/?plateNumber="+this.plateNumber;
    this.http.get<Vehicle[]>(url).subscribe(data=>{this.vehicles=data;console.log(data)});
  }

  confirm(){
    var url1 = "http://localhost:8080/api/book?plateNumber="+this.plateNumber+"&startDate="+this.startDate+"&endDate="+this.endDate;
    this.http.get<any>(url1).subscribe(data=>{this.status=data;console.log(data)});
    alert("You have sucessfully booked vehicle")
    this.router.navigate(
      ['/']
    )

  }

}

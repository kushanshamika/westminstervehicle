import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  nameFormControl = new FormControl();

  passwordFormControl = new FormControl();

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(private http: HttpClient, public router:Router) { }

  ngOnInit() {
  }

  register(){

    var name = this.nameFormControl.value;
    var mail = this.emailFormControl.value;
    var password = this.passwordFormControl.value;

    let body = new HttpParams({

      fromObject:{
        'name':name,
        'mail':mail,
        'password':password
      }
    });

    var url = 'http://ec2-18-221-114-73.us-east-2.compute.amazonaws.com:8080/api/reg'

    this.http.post<any>(url,body).subscribe(
      data=>{
        alert("You have succesfully registered")
        this.router.navigate(
          ['/login']
        );
      }
    )
  }

}

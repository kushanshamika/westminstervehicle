import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionService } from './session';

import {LoginComponent} from  './login/login.component'
import {HomeComponent} from './home/home.component'
import {RegisterComponent} from './register/register.component'
import {ResultComponent} from './result/result.component'
import {BookComponent} from './book/book.component'
import { ConfirmComponent } from './confirm/confirm.component';


const routes: Routes = [
  {path:'login',component: LoginComponent},
  {path:'',component:HomeComponent,canActivate:[SessionService]},
  {path:'register',component:RegisterComponent},
  {path:'result',component:ResultComponent,canActivate:[SessionService]},
  {path:'book',component:BookComponent,canActivate:[SessionService]},
  {path:'confirm',component:ConfirmComponent,canActivate:[SessionService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router ,ActivatedRoute, Routes} from '@angular/router';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  startDateControl = new FormControl();
  endDateControl = new FormControl();
  vehCategory = new FormControl();

  constructor(private datePipe: DatePipe,public router:Router) { }

  ngOnInit() {
  }

  search(){
    
    var cat = this.vehCategory.value;
    var start = this.datePipe.transform(this.startDateControl.value,"yyyy-MM-dd");
    var end  = this.datePipe.transform(this.endDateControl.value,"yyyy-MM-dd");

    this.router.navigate(
      ['/result'],
      {queryParams:{vehicleType:cat,startDate:start,endDate:end}}
    )
  }

}

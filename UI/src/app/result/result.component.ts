import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {HttpClient} from '@angular/common/http'
import { Url } from 'url';



interface Vehicle {
  vehicleType:String;
  plateNumber:String;
  vehicleModel: String;
  engineCapacity:String;
  availability:string;
  fuelType:String;
  pricePerKM: string;
  vehiclePaint:String;
  noOfSeats: string;
  vehicleMake: string;
}
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'], 
})

export class ResultComponent implements OnInit {

  vehicles:Vehicle[]=[]
 

  constructor(private http:HttpClient ,public route: ActivatedRoute,public router: Router) { }

  vehicleType:String;
  startDate:String;
  endDate:String;
  

  ngOnInit() {

    
    
    this.route.queryParams.subscribe(params=>{
      this.vehicleType = params['vehicleType'];
      this.startDate = params['startDate'];
      this.endDate = params['endDate']

    });

    var url = "http://localhost:8080/api/search?vehicleType="+this.vehicleType+"&startDate="+this.startDate+"&endDate="+this.endDate;
    this.http.get<Vehicle[]>(url).subscribe(data=>{this.vehicles=data;console.log(data)});

    

  }


  book(plateNumber:string){
    this.router.navigate(
      ['/confirm'],{queryParams:{plateNumber:plateNumber,startDate:this.startDate,endDate:this.endDate}}
    )
  }


}


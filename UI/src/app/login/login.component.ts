import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SessionService } from '../session';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  passwordFormControl = new FormControl();

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  

  constructor(private http: HttpClient,private session: SessionService, public router:Router) { }

  ngOnInit() {
  }

  login(){
    var mail = this.emailFormControl.value;
    var password = this.passwordFormControl.value;

    let body = new HttpParams({

      fromObject:{
        'mail':mail,
        'password':password
      }
    });

    var url = "http://ec2-18-221-114-73.us-east-2.compute.amazonaws.com:8080/api/login";

    this.http.post<any>(url,body).subscribe(
      data =>{
        if(data['auth']){
          this.session.setAuth(true);
          this.session.setID(data['_id']);
          this.session.setName(data['name']);

          this.router.navigate(
            ['']
          );
        }else{
          alert("Invalid Username or Password")
        }
      }
    )
  }

}
